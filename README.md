# Setup homelab

This is for setting up my homelab with:
- Gitlab
- Caddy

## Tools
- [just](https://github.com/casey/just)
- [docker](https://github.com/docker/compose)
- [sd](https://github.com/chmln/sd)
- [xdotool](https://github.com/jordansissel/xdotool)
- [xclip](https://github.com/astrand/xclip)
- [neovim](https://github.com/neovim/neovim)
- [caddy](https://github.com/caddyserver/caddy)

## Getting started
``` shell
git clone git@gitlab.com:the-integrative-driver-config/setup-homelab.git 
```

## How to use
You can configure everything easily using [just](https://just.systems/).
``` bash
Available recipes:
    build                    # build your caddy container
    default                  # get interactive recipe chooser
    deploy                   # deploy homelab
    docs                     # copy recipes docs
    exec service             # execute bash in service
    logs name                # show logs service
    ls                       # list all services
    passwd                   # get initial root password
    register token           # register token
    remove                   # kill and remove homelab
    service name replica="1" # get service container
    web                      # open website
```

## Configuration

The only thing you need to do is:
- Change root password 
