# get interactive recipe chooser
default:
  @just --choose

# build your caddy container
build:
  docker build -t homelab_caddy .

# deploy homelab 
deploy: build
  #!/bin/bash
  docker stack deploy homelab --compose-file docker-compose.yml

# get service container
service name replica="1":
  #!/bin/bash
  docker ps -f "name=homelab_{{name}}.{{replica}}" --format="{{{{.Names}}"

# show logs service 
logs name:
  #!/bin/bash
  docker logs "$(just service {{name}})"

# execute bash in service
exec service:
  #!/bin/bash
  docker exec -it $(just service {{service}}) bash

# list all services
ls:  
  #!/bin/bash
  docker service ls

# get initial root password
passwd: && web
  #!/bin/bash
  docker exec "$(just service gitlab)" grep 'Password:' /etc/gitlab/initial_root_password \
    | sd 'Password: ' '' \
    | xclip -sel clip
  
# kill and remove homelab 
remove:
  #!/bin/bash
  docker stack rm homelab

# register token 
register token:
  #!/bin/bash
  echo 'gitlab-runner register --non-interactive --executor "docker" --url "http://gitlab" -r {{token}}' | xclip -sel copy
  just exec gitlab-runner

# copy recipes docs
docs:
  #!/bin/bash
  just -l | xclip -sel clip
  nvim README.md +/Available recipes:

# open homebox
homebox:
  firefox http://homebox.localhost:3100
  xdotool key alt+1

# open gitlab 
gitlab:
  firefox https://localhost
  xdotool key alt+1
